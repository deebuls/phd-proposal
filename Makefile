filename=00PhdProposal
.PHONY: pdf clean-log
#pdf: ps
#	ps2pdf ${filename}.ps
pdf-clean: pdf clean-log
pdf:
	pdflatex ${filename}
	bibtex ${filename}||true
	pdflatex ${filename}
	pdflatex ${filename}


pdf-print: ps
	ps2pdf -dColorConversionStrategy=/LeaveColorUnchanged -dPDFSETTINGS=/printer ${filename}.ps

text: html
	html2text -width 100 -style pretty ${filename}/${filename}.html | sed -n '/./,$$p' | head -n-2 >${filename}.txt

html:
	@#latex2html -split +0 -info "" -no_navigation ${filename}
	htlatex ${filename}

ps:	dvi
	dvips -t a4 ${filename}.dvi


read:
	evince ${filename}.pdf &

aread:
	acroread ${filename}.pdf

clean:
	\rm -f *.{ps,pdf,log,aux,out,dvi,blg}

clean-log:
	rm -f *.ps *.log *.aux *.out *.dvi *.toc *.blg
