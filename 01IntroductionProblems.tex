\section{Introduction and Motivation}\label{introduction-and-motivation}
\begin{figure}[b] 
        \centering
        \vspace{0pt}
    \includegraphics[width=0.6\textwidth]{images/dependability.png}
        \caption{Dependability attributes for autonomous systems from \cite{lussier_fault_2004}}
    \label{fig:dependability}
\end{figure}
\textcolor{red}{
A Learning enabled autonomous system is a system whose behaviour is driven by
``background knowledge" acquired and updated by a ``learning process". This
definition encompasses a wide variety of popular machine learning and
algorithms}. Amongst these approaches, Deep Neural Networks (DNN) have
outperformed classical algorithms when it comes to tasks involving huge amount
of data from natural processes like image~\cite{krizhevsky2012imagenet},
speech~\cite{graves2013speech}, text~\cite{brown2020language} etc. As compared
to classical algorithms and traditional machine learning methods DNN‘s are
quite simple in design. Each DNN consist of an architecture called the model
and its corresponding weights~\cite{goodfellow2016deep}. The standard workflow
of DNN based software development is different from classical algorithms. The
DNN development workflow consist of data collection, data labeling,
architecture design, training and testing. This process is repeated and fine
tuned based on training and testing performance~\cite{bishop2006pattern}.  In
traditional software development we implement features based on requirements
and specifications
while in DNN (and general ML-approaches) we have data and let the DNN learn the
feature (functionality).

The capability of the deep learning methods to solve problems effectively with
data from natural processes and  highly unstructured environments makes it an
apt solution for robotic and autonomous systems (RAS). RAS are systems which can
accomplish goals independently, or with minimal supervision from human operators
in environments that are complex, unstructured and unpredictable. An autonomous
system is characterized by fundamental properties like functionality,
performance, dependability , security and cost \cite{lussier_fault_2004}.
For deploying an autonomous system in real world it has to balance 
between these fundamental properties.


Unfortunately, while it is easier than ever to design and train DNN,
incorporating them in autonomous systems remains challenging. One of the
challenge is that the DNN are not able to provide the assured service with
reliability which causes failures during execution. For example consider the
case of autonomous cars with DNN; there have been some instances where the
autonomous cars have resulted in fatal accidents. The reasons for these
accidents has been blamed on the changes in the environment and rarity of the
event
\footnote{https://www.tesla.com/blog/what-we-know-about-last-weeks-accident}.
Another example is when DNN are incorporated in RAS for
industrial manipulation \cite{fig:sub-second}. Here the robot picked up a beer
can in place of an profile which was caused because of faulty DNN-based image
recognition. 

\begin{figure}[t]
\begin{subfigure}{.5\textwidth}
  \centering
  % include first image
  \includegraphics[width=.9\linewidth]{images/beer-picking.png}  
  \caption{Robot picking can instead of profile }
  \label{fig:sub-first}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include second image
  \includegraphics[width=\linewidth]{images/car-crash.jpg}  
  \caption{Autonomous car crash with trailer \footnotemark }
  \label{fig:sub-second}
\end{subfigure}
\caption{Failures in DNN when incorporated in robotic and autonomous systems}
\label{fig:Introfig}
\end{figure}
\footnotetext{https://www.taiwannews.com.tw/en/news/3943199}

One of the hypothesis for fragile incorporation of
DNN in RAS is the optimization of DNN only for the functionality
property. Such a focus leads to corresponding design choices in DNN
development for example, DNN architecture selection, loss function selection,
probability selection etc.  \cite{sculley_pace_2018} . In-order to deploy the
DNN in addition to functionality the DNN has to be optimized for other
fundamental properties. In this thesis we focus on the \textit{dependability}
property of DNN-based components of robotic and autonomous systems. By making
DNN-based components more dependable we aim at the same time to 
increase dependability attributes such as availability, reliability, safety and 
maintainability of robotic and autonomous systems.

%the dependability of  Dependability of a system encompass the following
%attributes:\textit{availability}, \textit{reliability}, \textit{safety},
%\textit{integrity} and \textit{maintainability}. 

%\textit{
%The goal of the thesis is to investigate the different strategies to
%improve robustness and reliability of DNN incorporated autonomous systems.
%}

\textcolor{red}{
While designing a RAS, the designer has to consider the requirements from three 
orthogonal elements: the environment, the robot platform(Hardware and software )
and the robot task. Thus for a particular RAS the designer has to make a design choice
in all these three elements. Each design choice imparts a constraint on the remaining 
elements. For example, consider the example of design choice in environment, in the case of home 
robot the environment is dynamic and unstrucutred while in industrial robot the 
environment is dynamic but structured. This requirement of the environment will add
constraints in the platform and the task design. Similary each design choice will have 
an impact on the dependability of the system. 
}

\textit{
The main goal of the thesis is to develop methods to
improve dependability of learning enabled robotic and autonomous systems by exploiting the constraints.
}


\section{Challenges}\label{challenges}
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.4\textwidth]{images/pick.png}
    \caption{Illustrative example of a sorting robot with camera sensor \protect \todo{Update image}}
    \label{fig:challenges}
\end{figure}

To illustrate the relevance of the challenges presented in the work, we
motivate our work using a general example of DNN in a RAS.
We consider the problem of a sorting robot where a robot manipulator
with camera has to sort objects based on its class. The robot uses the
RGB sensor to perceive the environment and predicts the class of each
object. In this task the perception component of the robot is
implemented as a DNN. The perception task is modeled as an object
recognition problem. Once the dataset is collected labeled and model is
trained with acceptable accuracy the model is ready deployed in real
world to sort objects.
However, as the recognition model is deployed in the real world the
models face different challenges which were not anticipated during benchmark
training. For example, during training phase there can be system level
constraints that the model needs to be deployed on embedded hardware,
this causes significant reduction in computer power available for
inference. Other example during training phase can be that there is a
system level constraint that the runtime of the network has to be
minimum; this constraint will result in smaller model architecture
causing decrease in accuracy. Once based on system constraints and task
constraints a model is finalized and trained there can be scenarios
during execution phase which can be challenging for example the objects
to be manipulated are slightly altered in the color and shape or the
color of the containers are changed from that of training. These
modifications which were not available during training causes a drop in
accuracy of the model and thereby not meeting the expected performance
of the sorting task.
RAS are susceptible to these challenges because of the long tail problem
in deep learning. The long tail problem is that the variation in the data
that can be observed by the robot is very high as compared to the amount of 
data collected for training. This is because of high dimension of the 
data and highly dynamic environments.
\begin{figure}[htbp] 
    \centering
    \includegraphics[width=0.8\textwidth]{images/ood-ds-aa.png}
    \caption{Illustrative example of scenarios of challenges in incorporating
    DNN in autonomous systems}
         \label{fig:intro}       
\end{figure}

Thus based on the motivated example we enumerate the different challenges 
in incorporation of DNN models which we aim to tackle in the proposed thesis. 

\begin{itemize}

\item
\textbf{C1: Domain shift}
Domain shift occurs when the execution data is different from the
training data. DNN are very sensitive to domain shifts. DNN learn
spurious correlations in the training data making it vulnerable to 
accuracy drop during execution.

\item
\textbf{C2: Out-of-distribution}
As autonomous systems are deployed ``\textit{in the wild}", there is no control
over the input. Out-of-distribution refers to the problem of distinguishing
between Normal (normal) input data and Abnormal (abnormal, outliers) input
data. Detecting out-of-distribution is the ability of the model to distinguish
between what it knows and what it doesn't knows. Deep learning models are
trained for doing a particular task on particular assumptions, but when
deployed in autonomous systems there are instances when the assumptions are not
adhered. When building robotic and autonomous systems which are not aware of
the OOD detection capabilities of DNN are clearly sensitive to system failures.

\item
\textbf{C3: Adversarial attack}
Adversarial attacks are inputs to deep learning models intentionally designed 
to cause the DNN to make a mistake \cite{szegedy_intriguing_2014}. Some of the 
attacks are optical illusions while some are flipping bits of pixels. In
adversarial attacks the inputs seem to be similar for humans even after the 
pertrubations but deep learning DNN models outputs are modified. Robustness to
adversarial attacks is an important factor for building trust on deploying 
DNN in autonomous systems.


\end{itemize}


\section{Problem Statement}\label{problem-formulation}

Amongst the different attributes for dependable autonomous system, in this
thesis we will focus on improving reliability of RAS.  This internally requires
for improvement in robustness of the DNN models incorporated in RAS.  The
primary focus of the thesis is thus to improve the robustness of DNN models and reliability
of RAS in production. It can be formulated as an optimization problem.

\textbf{Incorporate DNN based perception in Autonomous Systems}
\begin{equation*}
\begin{aligned}
& {\text{maximize}}
& & \text{robustness of DNN} \\
& & & \text{reliability of autonomous system} \\
& \text{subject to}
& & \text{minimal operation cost}
\end{aligned}
\end{equation*}


\begin{definition}{\textbf{ Robustness }}
is the capability of a DNN to cope with errors during training and cope 
with erroneous input(out of distribution, adversarial ) during execution.
\end{definition}
\begin{definition}{\textbf{ Reliability}}
is continuity of correct service \cite{avizienis_basic_2004}.
 In our DNN based system it is the  capability of the system to provide no 
degradation of service even in case of faulty sensor inputs and subsequently faulty 
DNN predictions.
\end{definition}

\begin{definition}{\textbf{Operation cost}}
In deep learning the operation cost can be defined as the efforts to collect dataset
, training time of the models and the hardware requirements for executing the model.
\end{definition}


The different challenges are mentioned above have an negative impact on the robustness 
of the system. 

\begin{figure}[htbp] 
    \centering
    \includegraphics[width=0.5\textwidth]{images/robustness-reduce-reasons.png}
    \caption{Relationship between robustness and the challenges}
    \label{fig:robust}       
\end{figure}


Let $\mathbb{P}$ be the training data distribution and $\mathbb{Q}$ be the execution
data distirbution. Let $x $ be the input data and $y = {1, \dots , C}$ be the labels.
Then our illustrative problem of object sorter can be defined as a DNN learning
the following $Pr(y|x)$ from $\mathbb{P}$.


Then the different problems faced by the DNN in execution are:


\begin{itemize}

\item
  \textbf{Adversarial input}: 

  $p(x) = q(x)$ and $p(y) = q(y)$
  $\text{Problem : }p(y|x) \neq q(y|x+\epsilon)$

 Here both the input distribution $\mathbb{P}$ and 
execution distribution $\mathbb{Q}$ are same. But a small change $\epsilon$ in the 
input(adversarial) causes the output$y$ label to change.
\item
  \textbf{OOD input}: 

  $p(x) \neq q(x)$ and $p(y) \neq q(y)$ $\text{Problem : } p(y|x) = q(y|x)$

Here the input distribution  $\mathbb{P}$ and 
execution distribution $\mathbb{Q}$ are different and even the labels are different.
The expected behaviour is that the predicted labels has to be different from the 
actual labels. The main problem with OOD is that during training the OOD labels are 
not available.
 
\item
  \textbf{Domain shift}: 

 $p(x) \neq q(x)$ and $p(y) = q(y)$ $\text{Problem : }p(y|x) \neq q(y|x)$

Here the input distribution  $\mathbb{P}$ and 
execution distribution $\mathbb{Q}$ are different but the label distribution are
same and hence the predicted labels are also expected to have the same distribution.

\end{itemize}

\begin{table}[]
    \centering
\begin{tabular}{|c|c|c|c|}
\hline
Problem & Context & Expected Behaviour & Problem \\ \hline
Adversarial attack &  $p(x) = q(x)$ & $p(y|x) = q(y|x+\epsilon)$ & $p(y|x) \neq q(y|x+\epsilon)$ \\ \hline
OOD & $p(x) \neq q(x)$ & $p(y|x) \neq q(y|x)$ & $p(y|x) = q(y|x)$ \\ \hline
Domain Shift & $p(x) \neq q(x)$ &  $p(y|x) = q(y|x)$  &  $p(y|x) \neq q(y|x)$ \\ \hline

\end{tabular}
\end{table}



\begin{table}[]
    \centering
\begin{tabular}{|c|c|c|}
\hline
 & $p(y) = q(y)$ &  $p(y) \neq q(y)$ \\ \hline
 $p(x) = q(x)$ & Adversarial Attack & Label Shift \\ \hline
 $p(x) \neq q(x)$ & Domain Shift & Out of Distribution \\ \hline

\end{tabular}
\end{table}


\textbf{Research Questions}
Based on the challenges and the problem formulation we can formulate the
research questions we would like to answer in the thesis.
\begin{itemize}

\item
  \textbf{RQ1:} \textit{What are the different elements of the design space of a robust DNN?}
    \begin{itemize}
    \item
      \textit{What factors of the design space influence the robustness of DNN?}
    \end{itemize}

\item
  \textbf{RQ2:} \textit{How can a DNN accurately learn about the uncertainty in
  the data and model?}
\item
  \textbf{RQ3:} \textit{How can we use the embodiement of the autonomous system to overcome the
degradation of the DNN?}
\item
  \textbf{RQ4:} \textit{What methodology can be adopted to develop robust DNN?}
\end{itemize}




